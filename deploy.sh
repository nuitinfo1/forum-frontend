#!/usr/bin/env bash

chmod -R 775 ./*
mkdir -p build

# prepare deployment
APP_NAME="forum"
BUILD_FILE_NAME='forum-0.0.1-SNAPSHOT.war' # @Clement change this
DEPLOY_FILE_NAME="$APP_NAME.war"
DOCKER_SERVICE_NAME="angular_forum-backend"

# To increase safety, you could define this variables as environmental variables on gitlab
SSH_USER="root"
SSH_SERVER="51.68.80.229" # eg: sample.co.mz

# setup ssh private key
echo "$SSH_KEY_FILE" > "/tmp/id_rsa"
SSH_KEY_FILE="/tmp/id_rsa"
chmod 600 ${SSH_KEY_FILE}

mkdir -p ~/.ssh/
touch ~/.ssh/known_hosts
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

echo -e "Restart docker service"
ssh -i ${SSH_KEY_FILE} ${SSH_USER}@${SSH_SERVER} "cd /root/deployer/forum-frontend && git pull origin master"

echo -e "Restart docker service"
ssh -i ${SSH_KEY_FILE} ${SSH_USER}@${SSH_SERVER} "docker service update --force ${DOCKER_SERVICE_NAME}"
