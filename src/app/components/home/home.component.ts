import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TopicService} from '../../services/topic.service';
import {Topic} from '../../models/Topic';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  subscription;
  topics: Topic[];


  constructor(private activatedRoute: ActivatedRoute,
              private topicService: TopicService) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.params.subscribe(params => {
      if (params.id !== undefined) {
        this.topicService.findByCategoryId(Number(params.id))
          .then(result => {
            this.topics = result;
          }, error => {
            console.error(error);
          });
      } else {
        this.topicService.getAll()
          .then(result => {
            this.topics = result;
          }, error => {
            console.error(error);
          });
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
