import {Component, OnDestroy, OnInit} from '@angular/core';
import {Message} from '../../models/Message';
import {MessageService} from '../../services/message.service';
import {TopicService} from '../../services/topic.service';
import {Topic} from '../../models/Topic';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit, OnDestroy {

  subscription;
  messages: Message[];
  topic: Topic;
  message: Message;

  constructor(private activatedRoute: ActivatedRoute,
              private messageService: MessageService,
              private topicService: TopicService) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.params.subscribe(params => {
      this.topicService.findById(Number(params.id))
      .then(result => {
        this.topic = result;

        this.message = {
          messageId: null,
          message: null,
          dateTime: null,
          topic: this.topic,
          user: null
        };
      }, error => {
        console.error(error);
      });

      this.messageService.findByTopicId(Number(params.id))
        .then(result => {
          this.messages = result;
        }, error => {
          console.error(error);
        });
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  postMessage() {
    this.messageService.addMessage(this.message)
      .then(result => {
        this.messages.push(result);
      }, error => {
        console.error(error);
      });
  }

  setMessage(event) {
    this.message.message = event.target.value;
  }
}
