import {Component, OnInit} from '@angular/core';
import {User} from './models/User';
import {UserService} from './services/user.service';
import {CategoryService} from './services/category.service';
import {Category} from './models/Category';
import {Topic} from './models/Topic';
import {TopicService} from './services/topic.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'nuitInfo';
  user: User;
  categories: Category[];
  isModalVisible = false;
  topic: Topic;

  constructor(private userService: UserService,
              private categoryService: CategoryService,
              private topicService: TopicService,
              private router: Router) { }

  ngOnInit() {
    this.userService.findById(1)
      .then(result => {
        this.user = result;
      }, error => {
        console.error(error);
      });

    this.categoryService.getAll()
      .then(result => {
        this.categories = result;
      }, error => {
        console.error(error);
      });
  }

  addNewTopic() {
    this.topicService.addTopic(this.topic)
      .then(result => {
        this.router.navigate(['/home']);
        this.isModalVisible = false;
      }, error => {
        console.error(error);
      });
  }

  getAllTopics() {
    this.router.navigate(['/home']);
  }

  getTopicsByCategory(categoryId: number) {
    this.router.navigate(['/home/', categoryId]);
  }
}
