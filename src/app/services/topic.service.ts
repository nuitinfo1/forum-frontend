import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Topic} from '../models/Topic';
import {Message} from '../models/Message';

@Injectable({
  providedIn: 'root'
})
export class TopicService {

  constructor(private http: HttpClient) { }

  public getAll(): Promise<Topic[]> {
    return new Promise<Topic[]>((resolve, reject) => {
      return this.http.get<Topic[]>(`http://api.nuitinfo.oprax.fr/api/topics/get`)
        .subscribe(result => {
          resolve(result)
        }, error => {
          reject(error)
        })
    });
  }

  public findById(idTopic: number): Promise<Topic> {
    return new Promise<Topic>((resolve, reject) => {
      return this.http.get<Topic>(`http://api.nuitinfo.oprax.fr/api/topics/get/${idTopic}`)
        .subscribe(result => {
          resolve(result)
        }, error => {
          reject(error)
        })
    });
  }

  public findByCategoryId(idCategory: number): Promise<Topic[]> {
    return new Promise<Topic[]>((resolve, reject) => {
      return this.http.get<Topic[]>(`http://api.nuitinfo.oprax.fr/api/topics/categories/${idCategory}`)
        .subscribe(result => {
          resolve(result)
        }, error => {
          reject(error)
        })
    });
  }

  public addTopic(topic: Topic): Promise<Topic> {
    return new Promise<Topic>((resolve, reject) => {
      return this.http.post<Topic>(`http://api.nuitinfo.oprax.fr/api/topics/add/`, topic)
        .subscribe(result => {
          resolve(result)
        }, error => {
          reject(error)
        })
    });
  }
}
