import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Message} from '../models/Message';
import {Topic} from '../models/Topic';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient) {
  }

  public findByTopicId(idTopic: number): Promise<Message[]> {
    return new Promise<Message[]>((resolve, reject) => {
      return this.http.get<Message[]>(`http://api.nuitinfo.oprax.fr/api/messages/topics/${idTopic}`)
        .subscribe(result => {
          resolve(result)
        }, error => {
          reject(error)
        })
    });
  }

  public addMessage(message: Message) : Promise<Message> {
    return new Promise<Message>((resolve, reject) => {
      return this.http.post<Message>(`http://api.nuitinfo.oprax.fr/api/messages/add/`, message)
        .subscribe(result => {
          resolve(result)
        }, error => {
          reject(error)
        })
    });
  }
}
