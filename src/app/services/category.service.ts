import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Category} from '../models/Category';
import {User} from '../models/User';
import {Topic} from '../models/Topic';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  public getAll(): Promise<Category[]> {
    return new Promise<Category[]>((resolve, reject) => {
      return this.http.get<Category[]>(`http://api.nuitinfo.oprax.fr/api/categories/get`)
        .subscribe(result => {
          resolve(result)
        }, error => {
          reject(error)
        })
    });
  }
}
