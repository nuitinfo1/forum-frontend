import { Injectable } from '@angular/core';
import {Topic} from '../models/Topic';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public findById(idUser: number): Promise<User> {
    return new Promise<User>((resolve, reject) => {
      return this.http.get<User>(`http://auth.nuitinfo.oprax.fr/users/${idUser}`)
        .subscribe(result => {
          resolve(result)
        }, error => {
          reject(error)
        })
    });
  }
}
