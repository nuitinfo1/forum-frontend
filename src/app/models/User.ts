export interface User {
  userId: number;
  username: string;
  isActive: boolean;
  isStaff: boolean;
  isSuperUser: boolean;
  dateJoined: Date;

}
