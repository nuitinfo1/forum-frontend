export interface Category {
  categoryId: number;
  categoryName: string;
  dateTime: Date;
}
