import {Topic} from './Topic';
import {User} from './User';

export interface Message {
  messageId: number;
  message: string;
  dateTime: Date;
  topic: Topic;
  user: User;
}
