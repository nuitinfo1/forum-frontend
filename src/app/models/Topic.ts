import {Category} from './Category';

export interface Topic {
  topicId: number;
  topicName: string;
  dateTime: Date;
  category: Category;
}
